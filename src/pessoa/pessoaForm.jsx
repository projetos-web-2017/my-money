import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field} from 'redux-form';
import { init } from './pessoaActions';
import  LabelAndInput  from '../common/form/labelAndInput';


class PessoaForm extends Component {

    render(){
        const { handleSubmit, readOnly, nomeAcao, color } = this.props;
        return (
            <form role='form' onSubmit={handleSubmit}>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        Dados Pessoais
                    </div>
                    <div className="panel-body">
                        <Field name='cpf' component={LabelAndInput} readOnly={readOnly} label='CPF'cols='12 4' placeholder='Informe o CPF' />
                        <Field name='foto' component={LabelAndInput} readOnly={readOnly} label='Foto' cols='12 4'placeholder='Anexe uma foto' />
                        <Field name='nome' component={LabelAndInput}readOnly={readOnly}label='Nome'cols='12 4'placeholder='Informe o nome'/>
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        Endereço
                    </div>
                    <div className="panel-body">
                        <Field name='cep' component={LabelAndInput}readOnly={readOnly}label='CEP'cols='12 4'placeholder='Informe o cep'type='number'/>
                        <Field name='codMunicipio' component={LabelAndInput}readOnly={readOnly}label='Código da Cidade'cols='12 4'placeholder='Selecione o código da cidade' />
                        <Field name='codLogradouro' component={LabelAndInput}readOnly={readOnly}label='Cód. Logradouro'cols='12 4'placeholder='Selecione o código de logradouro' />
                        <Field name='logradouro' component={LabelAndInput}readOnly={readOnly}label='Logradouro'cols='12 4'placeholder='Informe o logradouro' />
                        <Field name='numero' component={LabelAndInput}readOnly={readOnly}label='Número'cols='12 4'placeholder='Informe o número'type='number' />
                        <Field name='complemento' component={LabelAndInput}readOnly={readOnly}label='Complemento'cols='12 4'placeholder='Informe o complemento' />
                        <Field name='bairro' component={LabelAndInput}readOnly={readOnly}label='Bairro'cols='12 4'placeholder='Informe o bairro' />
                    </div>
                </div>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        Dados Pessoais
                    </div>
                    <div className="panel-body">
                        <Field name='celular' component={LabelAndInput}readOnly={readOnly}label='Celular'cols='12 3'placeholder='Informe o celular'type='number' />
                        <Field name='telefone' component={LabelAndInput}readOnly={readOnly}label='Telefone'cols='12 3'placeholder='Informe o telefone'type='number' />
                        <Field name='empresa' component={LabelAndInput}readOnly={readOnly}label='Empresa'cols='12 3'placeholder='Informe a empresa' />
                        <Field name='departamento' component={LabelAndInput}readOnly={readOnly}label='Departamento'cols='12 3'placeholder='Informe o departamento' />
                        <Field name='email' component={LabelAndInput}readOnly={readOnly}label='Email'cols='12 4'placeholder='Informe o Email' />
                        <Field name='senha' component={LabelAndInput}readOnly={readOnly}label='Senha'cols='12 4'placeholder='Informe a Senha'type='password' />
                    </div>
                </div>
                <div className='box-footer'>
                    <button type='submit' className={`btn btn-${color}`}>{nomeAcao}</button>
                    <button type='button' className='btn btn-default' onClick={this.props.init}>Cancelar</button>
                </div>
            </form>
        )
    }
}

PessoaForm = reduxForm({ form: 'pessoaForm', destroyOnUnmount: false })(PessoaForm);
const mapDispatchToProps = dispatch => bindActionCreators({ init }, dispatch);

// export default connect(mapStateToProps, mapDispatchToProps)(BillingCycleForm);
export default connect(null, mapDispatchToProps)(PessoaForm);