import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import ContentHeader from '../common/template/contentHeader';
import Content from '../common/template/content';

import Tabs from '../common/tab/tabs';
import TabsContent from '../common/tab/tabsContent';
import TabsHeader from '../common/tab/tabsHeader';
import TabHeader from '../common/tab/tabHeader';
import TabContent from '../common/tab/tabContent';
import { selectTab, showTabs } from '../common/tab/tabActions';
import { createUser, updateUser, removeUser } from './pessoaActions';
import PessoaList from './pessoaList';
import PessoaForm from './pessoaForm';

class Pessoa extends Component {

    componentWillMount() {
        this.props.selectTab('tabList');
        this.props.showTabs('tabCreate', 'tabList');
    }

    render() {
        return (
            <div>
               <ContentHeader title='Pessoa' small='Cadastro' />
               <Content >
                   <Tabs>
                       <TabsHeader>
                            <TabHeader label='Listar' icon='bars' target='tabList' />
                            <TabHeader label='Incluir' icon='plus' target='tabCreate' />
                            <TabHeader label='Alterar ' icon='pencil' target='tabUpdate' />
                            <TabHeader label='Excluir' icon='trash-o' target='tabDelete' />
                       </TabsHeader>
                       <TabsContent>
                           <TabContent id='tabList'> 
                               <PessoaList />
                           </TabContent>
                           <TabContent id='tabCreate'> 
                               <PessoaForm onSubmit={this.props.createUser} nomeAcao='Criar' color='primary' />
                           </TabContent>
                           <TabContent id='tabUpdate'> 
                                <PessoaForm onSubmit={this.props.updateUser} nomeAcao='Alterar' color='warning' />
                           </TabContent>
                           <TabContent id='tabDelete'> 
                               <PessoaForm onSubmit={this.props.removeUser} readOnly nomeAcao='Remover' color='danger' />
                           </TabContent>
                       </TabsContent>
                    </Tabs>
                </Content>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tab: state.tab,
    billingList: state.billingList
});
const mapDispatchToProps = dispatch => bindActionCreators({
    selectTab, showTabs, createUser, updateUser, removeUser }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Pessoa);
