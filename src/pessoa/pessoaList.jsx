import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getPessoaList, showUpdate, showDelete } from './pessoaActions';


class PessoaList extends Component {

    componentWillMount() {
        this.props.getPessoaList();
    }

    renderRows() {
        const list = this.props.userList || [];
        return list.map((p, i) => (
            <tr key={i}>
                <td>{p.nome}</td>
                <td>{p.cpf}</td>
                <td>{p.email}</td>
                <td>{p.empresa}</td>
                <td>{p.departamento}</td>
                <td> 
                    <button className='btn btn-warning' onClick={() => this.props.showUpdate(p)}>
                        <i className='fa fa-pencil'></i>
                    </button>
                    <button className='btn btn-danger' onClick={() => this.props.showDelete(p)}>
                        <i className='fa fa-trash'></i>
                    </button>
                </td>
            </tr>
        ));
    }

    render() {
        console.log(this.props.userList);
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Email</th>
                            <th>Empresa</th>
                            <th>Departamento</th>
                            <th className='table-actions'>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    tab: state.tab,
    userList: state.pessoa.userList
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getPessoaList, showUpdate, showDelete }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(PessoaList);
