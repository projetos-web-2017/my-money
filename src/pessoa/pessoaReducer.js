const INITIAL_STATE = { userList: [] };

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'USER_LIST':
            return { ...state, userList: action.payload.data };
        default: 
            return state;
    }
};
