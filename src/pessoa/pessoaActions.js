import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import { reset as resetForm, initialize } from 'redux-form';
import { showTabs, selectTab } from '../common/tab/tabActions';
const URL = 'http://localhost:3003/api';
const INICITAL_VALUES = {};

export const getPessoaList = () => {
    const request = axios.get(`${URL}/pessoas`);
    return {
        type: 'USER_LIST',
        payload: request
    };
};

export const createUser = (values) => {
    return submit(values, 'post', 'criado');
};

export const updateUser = (values) => {
    return submit(values, 'put', 'alterado');
};

export const removeUser = (values) => {
    return submit(values, 'delete', 'removido');
};

const submit = (values, method, frase) => {
    return dispatch => {
        const id = values._id ? values._id : '';
        axios[method](`${URL}/pessoas/${id}`, values)
            .then(() => {
                toastr.success('Sucesso', 'Pessoa ' + frase + ' com sucesso');
                dispatch(init());
            })
            .catch(error => {
                error.response.data.errors.forEach(e => toastr.error('Erro', e));
            });
    };
};

export const showUpdate = (user) => {
    return [
        showTabs('tabUpdate'),
        selectTab('tabUpdate'),
        initialize('pessoaForm', user)
    ];
};

export const showDelete = (user) => {
    return [
        showTabs('tabDelete'),
        selectTab('tabDelete'),
        initialize('pessoaForm', user)
    ];
};

export const init = () => {
    return [
        showTabs('tabList', 'tabCreate'),
        selectTab('tabList'),
        getPessoaList(),
        initialize('pessoaForm', INICITAL_VALUES)
    ];
};

