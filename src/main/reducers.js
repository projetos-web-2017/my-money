//dependencias para reducers
import { combineReducers } from 'redux'; 
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';

//reducers da aplicacao
import dashboard from '../dashboard/dashboardReducer';
import billingCycle from '../billingCycle/billingCycleReducer';
import pessoa from '../pessoa/pessoaReducer';
import tab from '../common/tab/tabReducer';
import auth from '../auth/authReducer'

const rootReducer = combineReducers({
    dashboard,
    billingCycle,
    pessoa,
    tab,
    auth,
    form: formReducer,
    toastr: toastrReducer
});

export default rootReducer;
