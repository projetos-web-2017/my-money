import React from 'react';
import { Router, Route, Redirect, hashHistory, IndexRoute } from 'react-router';

import AuthOrApp from './authOrApp'
import Dashboard from '../dashboard/dashboard';
import BillingCycle from '../billingCycle/billingCycle';
import Pessoa from '../pessoa/pessoa';


export default props => (
    <Router history={hashHistory}>
        <Route path='/' component={AuthOrApp} >
            <IndexRoute component={Dashboard}/>
            <Route path='billingCycles' component={BillingCycle} />
            <Route path='pessoa' component={Pessoa} />
        </Route>
        <Redirect from='*' to='/' />
    </Router>
);
