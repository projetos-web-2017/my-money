const INITIAL_STATE = { billingList: [] };

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'BILLING_LIST':
            return { ...state, billingList: action.payload.data }
        default: 
            return state;
    }
};
