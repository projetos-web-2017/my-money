import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getBillingCycleList, showUpdate, showDelete } from './billingCycleActions';


class BillingCycleList extends Component {

    componentWillMount(){
        this.props.getBillingCycleList()
    }

    renderRows(){
        const list = this.props.billingList || [];
        return list.map((bc, i) => (
            <tr key={i}>
                <td>{i}</td>
                <td>{bc.name}</td>
                <td>{bc.month}</td>
                <td>{bc.year}</td>
                <td> 
                    <button className='btn btn-warning' onClick={() => this.props.showUpdate(bc)}>
                        <i className='fa fa-pencil'></i>
                    </button>
                    <button className='btn btn-danger' onClick={() => this.props.showDelete(bc)}>
                        <i className='fa fa-trash'></i>
                    </button>
                </td>
            </tr>
        ))
    }

    render(){
        console.log(this.props.billingList)
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Mes</th>
                            <th>Ano</th>
                            <th className='table-actions'>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    tab: state.tab,
    billingList: state.billingCycle.billingList
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getBillingCycleList, showUpdate, showDelete }, dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(BillingCycleList);