import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import { reset as resetForm, initialize } from 'redux-form';
import { showTabs, selectTab } from '../common/tab/tabActions';

const URL = 'http://localhost:3003/api';
const INICITAL_VALUES = {};

export const getBillingCycleList = () => {
    const request = axios.get(`${URL}/billingCycles`);
    return {
        type: 'BILLING_LIST',
        payload: request
    };
};

export const create = (values) => {
    return submit(values, 'post', 'criado');
};

export const update = (values) => {
    return submit(values, 'put', 'alterado');
};

export const remove = (values) => {
    return submit(values, 'delete', 'removido');
};

const submit = (values, method, frase) => {
    return dispatch => {
        const id = values._id ? values._id : '';
        axios[method](`${URL}/billingCycles/${id}`, values)
            .then(() => {
                toastr.success('Sucesso', 'Ciclo de pagamento ' + frase + ' com sucesso');
                dispatch(init());
            })
            .catch(error => {
                error.response.data.errors.forEach(e => toastr.error('Erro', e));
            });
    };
};

//*** reusar o codigo para mostrar abas */

export const showUpdate = (billingCycle) => {
    return [
        showTabs('tabUpdate'),
        selectTab('tabUpdate'),
        initialize('billingCycleForm', billingCycle)
    ];
};

export const showDelete = (billingCycle) => {
    return [
        showTabs('tabDelete'),
        selectTab('tabDelete'),
        initialize('billingCycleForm', billingCycle)
    ];
};

export const init = () => {
    return [
        showTabs('tabList', 'tabCreate'),
        selectTab('tabList'),
        getBillingCycleList(),
        initialize('billingCycleForm', INICITAL_VALUES)
    ];
};
